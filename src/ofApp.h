#pragma once

#include "ofMain.h"
#include <ofxSvg.h>
#include <ofxPolylineMerger.h>
#include <ofxBlur.h>
#include <ofxPanel.h>


class ofApp : public ofBaseApp{
	private:
		ofxSVG svg;
		ofxPolylineMerger interpolator;
		ofxBlur blur;
		
		ofxPanel gui;
		ofxSlider<float> blurScale;
		ofxSlider<int> intSteps;

		float interpolateCoeff;
		bool bDrawBlurry = false;
		bool bDrawLines = true;

		void onSliderChanged(float& val);
		void onIntSliderChanged(int& val);

	public:
		void setup();
		void update();
		void draw();
	void calculateInterpolation();
	void generateMesh();
	void applyBlur();
	void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
};
