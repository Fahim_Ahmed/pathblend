#include "ofApp.h"


int sw, sh;
int steps;

ofPolyline p1;
ofPolyline p2;

vector<ofPolyline> lines;
vector<ofMesh> shapes;

int linesSize;
int shapesSize;

float lastTime;
float interpolateTime;
float _time = 0;
float theta = 0;

ofFloatColor c0 = ofFloatColor::fromHex(0xffe29d);
ofFloatColor c1 = ofFloatColor::fromHex(0xffb919);
ofFloatColor c2 = ofFloatColor::fromHex(0xd50cff);


//--------------------------------------------------------------
void ofApp::setup(){
	ofBackground(255);
	ofEnableAntiAliasing();
	ofSetFrameRate(60);

	sw = ofGetWidth();
	sh = ofGetHeight();
	ofSetLineWidth(1);

	svg.load("sample2.svg");	

	p1 = svg.getPathAt(0).getOutline()[0]; // D
	p2 = svg.getPathAt(1).getOutline()[0]; // A
		
	interpolator.setup();
	steps = 6;

	blur.setup(sw, sh, 32, 0.2, 1, 0.5);
	blur.setScale(0.1);

	calculateInterpolation();
	generateMesh();
	applyBlur();

	gui.setup();
	gui.add(blurScale.setup("Blur Scale", 0.1f, 0.0, 4.0f));
	gui.add(intSteps.setup("Interpolation", steps, 2, 24));

	blurScale.addListener(this, &ofApp::onSliderChanged);
	intSteps.addListener(this, &ofApp::onIntSliderChanged);
}

void ofApp::onSliderChanged(float &val) {
	blur.setScale(val);
//	calculateInterpolation();
//	generateMesh();
	applyBlur();
}
void ofApp::onIntSliderChanged(int &val) {
	steps = val;
	calculateInterpolation();
	generateMesh();

	if (bDrawBlurry) applyBlur();
}

//--------------------------------------------------------------
void ofApp::update(){
	_time = ofGetElapsedTimef() - lastTime;
	if (_time>interpolateTime*2.) {
		lastTime = ofGetElapsedTimef();
	}

	interpolateCoeff = _time / interpolateTime;
	if (interpolateCoeff>1.) {
		interpolateCoeff = 2. - interpolateCoeff;
	}

	interpolator.mergePolyline(p1, p2, interpolateCoeff);
}


//--------------------------------------------------------------
void ofApp::draw(){
	
	if (bDrawBlurry) {
		blur.draw();
	}
	else {

		ofPushMatrix();
		ofTranslate(sw * 0.5f, sh * 0.5f);
		ofScale(3, 3);

		ofSetColor(255);
		//	ofEnableAlphaBlending();

		for (int i = shapesSize - 1; i >= 0; i--) {
			ofMesh m = shapes.at(i);

			//ofPushMatrix();
			//ofRotate((i % 2 == 0) ? theta : (-1 * theta));
			m.draw();
			//ofPopMatrix();
		}

		if (bDrawLines) {
			for (int i = linesSize - 1; i >= 0; i--) {
				ofPolyline p = lines.at(i);

				//ofPushMatrix();
				//ofRotate((i%2==0) ? theta : (-1*theta));
				p.draw();
				//ofPopMatrix();
			}
		}

		//	ofDisableAlphaBlending();

		ofPopMatrix();
	}

	gui.draw();

//	theta += 0.5f;
//	if (theta >= 360) theta = 0;
}

void ofApp::calculateInterpolation() {
	//Interpolate
	interpolator.setNbPoints(400);

	interpolateCoeff = 0.0;
	float inc = 1 / float(steps);

	lines.clear();

	for (int i = 0; i <= steps; i++) {
		interpolator.mergePolyline(p2, p1, interpolateCoeff);
		ofPolyline p = interpolator.getPolyline();
		p.setClosed(true);
		lines.push_back(p);

		interpolateCoeff += inc;
	}

	linesSize = lines.size();
}

void ofApp::generateMesh() {
	//Generate Mesh
	shapes.clear();

	for (int i = 0; i < lines.size() - 1; i++) {

		ofMesh m;

		for (int j = 0; j < 2; j++) {
			ofPolyline poly = lines.at(i + j);
			int len = poly.getVertices().size();

			for (int k = 0; k < len; k++) {
				m.addVertex(poly.getVertices().at(k));
				//m.addColor((j == 0) ? ofFloatColor::fromHex(0xffb919) : ofFloatColor::fromHex(0xd50cff));
				if (i + j == 0) m.addColor(c0);
				else m.addColor(c1.getLerped(c2, float(i + j) / float(lines.size())));
			}
		}

		m.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
		int len = m.getVertices().size() * 0.5f;
		for (int j = 0; j < len; j++) {
			int a = j;
			int b = (j == len - 1) ? 0 : j + 1;
			int c = j + len;
			int d = (c == len * 2 - 1) ? len : (j + len + 1);

			m.addIndex(a);
			m.addIndex(b);
			m.addIndex(c);

			m.addIndex(c);
			m.addIndex(b);
			m.addIndex(d);
		}

		shapes.push_back(m);
	}

	shapesSize = shapes.size();
}

void ofApp::applyBlur() {
	//Apply filter
	blur.begin();
	ofSetColor(255);

	ofSetColor(255);
	ofDrawRectangle(0, 0, sw, sh);

	ofPushMatrix();
	ofTranslate(sw * 0.5f, sh * 0.5f);
	ofScale(3, 3);

	for (int i = shapes.size() - 1; i >= 0; i--) {
		ofMesh m = shapes.at(i);
		m.draw();
	}

	ofPopMatrix();

	blur.end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == 'b' || key == 'B')
		bDrawBlurry = !bDrawBlurry;
	if (key == 'w')
		bDrawLines = !bDrawLines;
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
